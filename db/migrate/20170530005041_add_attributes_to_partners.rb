# generated migration files
class AddAttributesToPartners < ActiveRecord::Migration[5.0]
  def change
    add_column :partners, :name, :string
    add_column :partners, :photo, :string
    add_column :partners, :github, :string
    add_column :partners, :gitlab, :string
    add_column :partners, :linkedin, :string
  end
end
