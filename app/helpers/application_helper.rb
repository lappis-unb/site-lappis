# most used links, methods, variable names, verifications to avoid repetition
module ApplicationHelper
  def navbar_links(*desired_links)
    links = { blog: content_tag(:li, link_to(font_awesome('newspaper-o') +
                                  ' Blog', '/blog')),
              about: content_tag(:li, link_to(font_awesome('users') +
                                  ' Sobre', '/#about-us')),
              products: content_tag(:li, link_to(font_awesome('line-chart') +
                                  ' Produtos', '/#products')),
              partners: content_tag(:li, link_to(font_awesome('heart') +
                                  ' Parceiros', '/#partners')),
              radar: content_tag(:li, link_to(font_awesome('bullseye') +
                                  ' Radar', '/radar')),

              # Not yet implemented
              # search: content_tag(:li, link_to(font_awesome('search'),
              #                      '#', class: 'search')),

              menu: content_tag(:li, link_to(font_awesome('bars'), '#')) }
    links.slice(*desired_links).values.join { |html| "#{html}" }
  end

  # helper for summoning a font-awesome icon
  # recieves the icon name as param
  def font_awesome (icon_name)
    content_tag(:span, nil, class: "fa fa-#{icon_name}")
  end

  # connects with facebook's plugin
  # used on article's page
  def facebook_comments_plugin
    "https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.9&appId
    =1185185261540232"
  end
end
