# eases the access to months names
module PagesHelper
  def months_names
    %w[
      janeiro
      fevereiro
      março
      abril
      maio
      junho
      julho
      agosto
      setembro
      outubro
      novembro
      dezembro
    ]
  end
end
